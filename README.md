﻿# Readme

This exercise involves creating a simple, responsive, 1-page ASP.NET 5 MVC web application that accepts a ZIP code (to be used later) and a temperature in degrees F. It will convert the temperature to degrees C.   No persistence is required. 

## Design process

1.  Determine **controller** requirements - how many? What will you name them?  
2.  Determine **view** requirements - how many? What will you name them?
3.  Determine **model** requirements - how many?  What will you name them?

## Implementation

1.  Create a **repository**.
2.  Create an **MVC project**.
3.  Create your **model**.  Add annotations and validations to get the required behavior. 
4.  Create your **controller**.  Implement the default Index action. We don't need scaffolding, since we won't be doing CRUD.
5.  Create your **view** (an example is provided below). 
6.  Implement the **Convert** action (called when the user submits the form).
7.  Clean up unneeded navigation in _Layout.cs.

## Desired behavior

![](images/app1.png) 

![](images/app2.png) 

![](images/app3.png) 

[video](images/app.swf)  


## Submission requirements

1.  Your app must be called "CoverterYourName" and your code must be kept in a repository. Invite the instructor and course assistants to your repo.
2.  You must use an MVC approach.
3.  Your controller must pass the title (which changes as shown in the video) and a result.
4.  Your controller must pass a model to the view.
5.  The zip code field must only accept valid US ZIP codes.
6.  The temperature field is required and must accept only reasonable ambient temperature values (in degrees F)
7.  You must display default values to the user.
8.  Include a README file (.md or .txt) and add a .gitignore to your repo.
9.  You can include your About page in all work.
10.  Reply with a link to your repository and the following 3 screen shots that show the app running in your IDE. 

The behavior must meet the requirements shown.  You may update styles, appearance, layout etc. as you wish. 

## Hints:

*   Your Index action could initialize the Title, Result, and a model.
*   The model should include valid default values. 
*   Your Convert action could reset the Title and Result.
*   Your Convert action can check if the ModelState is valid.
*   If it is, it should update the title and result.  
*   Valid or not, it could return the Index view (along with the model). 

----------------------------------------------------------------------------------------------

## Step 1 - Design


Determine **controller** requirements - how many? What will you name them?  

* _Only one screen in the app... we don't have much content... I'll start with just one... a **HomeController**._

Determine **view** requirements - how many? What will you name them?

* _Only one screen ..just the default view so I'll call it **Index**..it has to be responsive, so I'll put it in a simplified **Layout** from the sample app... The title at the top and the result at the bottom could be strings sent by ViewData....I want validation on the user inputs, so I'll use a model for those._

Determine **model** requirements - how many?  What will you name them?

* _Looks like one model.... for ZIP code and Temperature in F ... I'll call it a **Converter.**_

----------------------------------------------------------------------------------------------

## Step 2 - Implementation



1.  Go to  [https://bitbucket.org/account/user/NW-44-663-2016/projects/A05](https://bitbucket.org/account/user/NW-44-663-2016/projects/A05). Create a repo **ConverterYourname** (or create a private one and invite us with write access). 

2.  Create a **new ASP.NET 5 Web Application project** (check add to source control if desired) and select No authentication.  Don't add Application Insights. For more help, see the  [docs ](https://docs.asp.net/en/latest/tutorials/your-first-aspnet-application.html#create-a-new-asp-net-5-project).

    Run your site, verify it works, make an initial commit, then push it to the repository

3.  Stop your application. Right-click on your project folder and add a subfolder called **Models**. Add a new class called **Converter.cs**. Add a primary key property and the two displayed properties. Use annotations to provide the validation required. Run your site, verify it works, commit to Master branch, then push to the repository.

4.  Stop your application. Now we'll work on the **controller**. Delete unnecessary actions. Use ViewData to set the default **Title** and **Result** strings (the result should be an empty string to start). Create a default Model and pass it to the view.

    Run your site, verify it works, commit to Master branch, then push to the repository.

5.  Stop your application. Now we'll work on the **view**. Delete unnecessary views. Delete the current contents of **Index.cshtml**. At the top, add a reference to our model.  

    We have 3 horizontal parts: Title, Input Form, Result. We can start as shown below. (Be sure to use your project name, not ConverterCase.)  

    ![](images/dev01.png) 

    Run your site, verify it works, make an initial commit, then push it to the repository.   

    Create a form using asp helper tags. To provide spacing between the two input fields and the button, create 3 form groups. [[view form] ](https://bitbucket.org/NW-44-663-2016/converterexample/raw/25d5fa0c0bf1dc201c26a0885fc77a68fa8e805a/src/ConverterCase/Views/Home/Index.cshtml)    

    ![](images/dev02.png) 

    Add a label, input, and validation feedback for zipcode to the first form group:

    ```
    <label asp-for="Zipcode" class="control-label"></label>  

    <input asp-for="Zipcode" class="form-control" />   

    <span asp-validation-for="Zipcode" class="text-danger"></span>    
    ``` 

     Do the same for the temperature property. Add a submit button with:

    ```
    <input type="submit" value="Convert" class="btn btn-info" />
    ``` 

    At the top of the form, we specify the controller and action called when the submit button is clicked. 

    Implement a Convert method in the HomeController that accepts a Converter model. 

    If validation succeeds, update the Title and Result. Return an Index view with the model.

    ![](images/dev03.png) 

    Bootstrap improves the appearance, so wrap the whole form in a div with bootstrap class="col-xs-12" so that each row will use all 12 columns on extra-small screens (and larger). Remember to close the div tag after the form ends. 

    ```
    <div class="col-xs-12"> 
    ``` 



6.  Our opening form tag specifies the controller and action to be called when the user submits the form. Create a new **Convert** action in the **HomeController** that accepts a **Converter** model. If validation succeeds, update **Title** and **Result**. Return an **Index** view with the model.

    ![](images/dev04.png)

7.  Clean up the unneeded navigation in _Layout.cs.  We don't need any of the navbar-collapse (with Home / About / Contact) and we don't the navbar-toggle.

    ![](images/dev05.png)


